<?php
session_start();

$name = $_SESSION["name"];
$gender = $_SESSION["gender"];
$faculty = $_SESSION["faculty"];
$birth = $_SESSION["birth"];
$address = $_SESSION["address"];
$image = $_SESSION["image"];

$genders = $_SESSION["genders"];
$faculties = $_SESSION["faculties"];
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="style.css">
    <title>Confirmation</title>
</head>

<body>
    <main>
        <form action="confirmation.php" method="post">
            <div class="container">
                <?php
                header("Content-type: text/html; charset=utf-8");
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $dbname = "formDB";

                    // // Create connection
                    // $conn = new mysqli($servername, $username, $password);
                    // // Check connection
                    // if ($conn->connect_error) {
                    //     die("Connection failed: " . $conn->connect_error);
                    // }

                    // // Create database
                    // $sql = "CREATE DATABASE IF NOT EXISTS formDB";
                    // if ($conn->query($sql) === TRUE) {
                    //     echo "Database created successfully</br>";
                    // } else {
                    //     echo "Error creating database: " . $conn->error . "</br>";
                    // }

                    // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
                    // Check connection
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    // Change character set to utf8
                    mysqli_set_charset($conn, 'UTF8');

                    // // Create table
                    // $sql = "CREATE TABLE IF NOT EXISTS `student` (
                    //     `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                    //     `name` varchar(250) NOT NULL,
                    //     `gender` int(1) NOT NULL,
                    //     `faculty` char(3) NOT NULL,
                    //     `birthday` datetime NOT NULL,
                    //     `address` varchar(250) DEFAULT NULL,
                    //     `avatar` text DEFAULT NULL
                    // ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                    // if ($conn->query($sql) === TRUE) {
                    //     echo "Table created successfully</br>";
                    // } else {
                    //     echo "Error creating table: " . $conn->error . "</br>";
                    // }

                    $birth = date("Y-m-d H:i:s", strtotime($birth));

                    $sql = "INSERT INTO student (name, gender, faculty, birthday, address, avatar)
                            VALUES ('$name', '$gender', '$faculty', '$birth', '$address', '$image') ";

                    if ($conn->query($sql) === TRUE) {
                        echo "New record created successfully</br>";
                        $status = TRUE;
                    } else {
                        echo  "Error: " . $sql . "<br>" . $conn->error . "</br>";
                        $status = FALSE;
                    }

                    $conn->close();

                    if ($status) {
                        header("location: complete_regist.php");
                    }
                }
                ?>
                <div>
                    <label for="name">Họ và tên</label>
                    <?php
                    echo $name . '</br>';
                    ?>
                </div>

                <div>
                    <label for="gender">Giới tính</label>
                    <?php
                    echo $genders[$gender] . '</br>';
                    ?>
                </div>

                <div>
                    <label for="faculty">Phân khoa</label>
                    <?php
                    echo $faculties[$faculty] . '</br>';
                    ?>
                </div>

                <div>
                    <label for="birth">Ngày sinh</label>
                    <?php
                    echo $birth . '</br>';
                    ?>
                </div>

                <div>
                    <label for="address">Địa chỉ</label>
                    <?php
                    echo $address . '</br>';
                    ?>
                </div>

                <div>
                    <label for="address">Hình ảnh</label>
                    <?php
                    if (file_exists($image)) {
                        echo "<img src=$image width='200px'>";
                    }
                    ?>
                </div>

                <button type="submit" class="button" name="register">Xác nhận</button>
        </form>
    </main>
</body>

</html>